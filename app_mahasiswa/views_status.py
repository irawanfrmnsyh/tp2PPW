from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .models import Status, Pengguna
from .forms import Status_Form

from.views import response

response = {}

def status(request):# pragma: no cover
	response['login_name'] = request.session.get('user_login')
	if 'user_login' in request.session:
		username = request.session.get('user_login', None)
		response['show_form'] = True
	else:
		response['show_form'] = False
	username = request.session.get('user_login')
	user = Pengguna.objects.get(username=username)
	status = Status.objects.filter(pengguna__username=username)
	response['foto_profil'] = user.foto_profil
	response['name'] = user.username
	response['status'] = status
	response['status_form'] = Status_Form
	response['model'] = status
	# html = 'session/mahasiswaPage.html'
	# return render(request, html, response)

def add_status(request):# pragma: no cover
	form = Status_Form(request.POST or None)
	if (request.method == 'POST' and form.is_valid()):
		response['isi'] = request.POST['status']
		username = request.session['user_login']
		user = Pengguna.objects.get(username=username)
		status = Status(pengguna=user,status=response['isi'])
		status.save()
		return HttpResponseRedirect('/mahasiswa/profile')
	else:
		return HttpResponseRedirect('/mahasiswa/profile')

def delete_status(request, id):# pragma: no cover
	Status.objects.filter(id=id).delete()
	return HttpResponseRedirect('/mahasiswa/profile')
# bikin views.py buat masing" fitur di sini ya, nanti kalo mau dipake ke urls.py jangan lupa di import dulu filenya
