# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-11 02:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Keahlian',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('keahlian', models.CharField(max_length=10, verbose_name='keahlian')),
                ('level', models.CharField(max_length=15, verbose_name='level')),
            ],
        ),
        migrations.CreateModel(
            name='Pengguna',
            fields=[
                ('kode_identitas', models.CharField(max_length=20, primary_key=True, serialize=False, verbose_name='npm')),
                ('nama', models.CharField(max_length=200, verbose_name='nama')),
                ('profile_linkedin', models.CharField(max_length=200, verbose_name='profile_linkedin')),
                ('foto_profil', models.CharField(max_length=240, verbose_name='profpic')),
                ('email', models.CharField(max_length=200, verbose_name='email')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.TextField()),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('pengguna', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_mahasiswa.Pengguna')),
            ],
        ),
        migrations.AddField(
            model_name='keahlian',
            name='pengguna',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_mahasiswa.Pengguna'),
        ),
    ]
