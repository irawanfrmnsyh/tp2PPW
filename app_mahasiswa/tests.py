from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index_root

# Create your tests here.
class Lab10UnitTest(TestCase):

    def test_url_is_redirected(self):
        response = self.client.get('/mahasiswa/')
        self.assertEqual(response.status_code, 302)

    def test_using_index_func(self):
        found = resolve('/mahasiswa/')
        self.assertEqual(found.func, index_root)